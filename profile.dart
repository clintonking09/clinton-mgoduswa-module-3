import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("User Profile"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          SizedBox(
            height: 100,
            width: 100,
            child: Stack(
              fit: StackFit.expand,
              clipBehavior: Clip.none,
              children: const [
                 CircleAvatar(
                  backgroundColor: Colors.grey,
                  backgroundImage: AssetImage("asserts/Clinton.jpeg"),
                )
              ],
            ),
          ),
          
          const Padding(
            padding: EdgeInsets.all(10.0),
            child: TextField(
            obscureText: false,
            decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Username',
  ),
),
          ),
          const Padding(
            padding: EdgeInsets.all(10.0),
            child: TextField(
  obscureText: true,
  decoration: InputDecoration(
    border: OutlineInputBorder(),
    labelText: 'Password',
  ),
),
          )
        ],
      ),
       floatingActionButton: FloatingActionButton(
        onPressed: () {

        },
        child: const Icon(Icons.edit),
    ));
  }
}
